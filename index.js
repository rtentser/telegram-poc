import { TelegramClient } from "telegram";
import { StringSession } from "telegram/sessions/index.js";
import "dotenv/config";
import input from "input";

(async () => {
  console.log("Loading interactive example...");
  const client = new TelegramClient(
    new StringSession(process.env.SESSION),
    parseInt(process.env.API_ID),
    process.env.API_HASH,
    {
      connectionRetries: 5,
    }
  );

  if (process.env.SESSION === "") {
    await client.start({
      phoneNumber: async () => await input.text("Please enter your number: "),
      phoneCode: async () =>
        await input.text("Please enter the code you received: "),
      onError: (err) => console.log(err),
    });
    console.log(client.session.save()); // Save this string to avoid logging in again
  } else {
    await client.connect();
  }

  console.log("You should now be connected.");
  await client.sendMessage("me", { message: "Fuck you!" });
  await client.destroy();
})();
